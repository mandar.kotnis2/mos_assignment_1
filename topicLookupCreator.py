import os
import xml.etree.ElementTree as ET
import nltk
import re
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

filepath = r'D:\Mechanics of Search\backup\topics'
outputFilePath = r'D:\Mechanics of Search\Assignment 1\output 2'
outputFileName2 = r'collectionsDetails{}.out'

invertedIndexCollections = {}

counter = 1

os.chdir(filepath)
stopwords = set(stopwords.words('english'))
lemmatizer = WordNetLemmatizer()
titleArray2 = []
unique = []

for item in os.listdir():
    itemTree = ET.parse(item)
    itemRoot = itemTree.getroot()
    queryId = itemRoot[0].text
    title = itemRoot[1].text
    
    unique = []
    
    ###Data Cleaning
    title = re.sub(r'[^\w\s]', '', title)
    
    
    ###Tokenization
    queryIdArray = [queryId]
    titleArray = nltk.word_tokenize(title)
    
    for i,word in enumerate(titleArray):
        titleArray[i] = titleArray[i].lower()
    
    shortTitleArray = []
    
    ###Remove Stop Words
    titleArray = [word for word in titleArray if not word in stopwords]
    
    ###Lemmatization        
    for word in titleArray:
        tempWord = lemmatizer.lemmatize(word)
        titleArray2.append(tempWord)
        shortTitleArray.append(tempWord)
            
    for word in shortTitleArray:
        if word not in unique:
            unique.append(word)       
    
    os.chdir(r'D:\Mechanics of Search\Assignment 1\output 2')
    for i in range (1,2):
        collectionsDetailsFile = outputFileName2.format(i)
        
        collectionsDetailsFileName = open(collectionsDetailsFile, 'r')
        
        readCollections = collectionsDetailsFileName.readlines()
        collectionsDetailsFileName.close()
        
        collectionsDetails = eval(readCollections[0])
        for currentCollection in collectionsDetails:
            for word in unique:
                if word in currentCollection[1]:
                    if word in invertedIndexCollections:
                        occ = invertedIndexCollections[word]
                        occ = occ + 1
                        invertedIndexCollections[word] = occ
                    else:
                        invertedIndexCollections[word] = 1
    os.chdir(filepath)
    print('{} of 162 done'.format(counter))
    counter = counter + 1
            
print(invertedIndexCollections)

os.chdir(outputFilePath)
outputFile = open('IDFLookupTopics.out', 'w')
outputFile.write(str(invertedIndexCollections) + "\n")
outputFile.close()
