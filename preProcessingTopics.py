import os
import xml.etree.ElementTree as ET
import nltk
import re
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer


#nltk.download('punkt')
#nltk.download('wordnet')
#nltk.download('stopwords')

filepath = r'D:\Mechanics of Search\backup\topics'
outputFilePath = r'D:\Mechanics of Search\Assignment 1\output 2'
outputFileName1 = r'invertedIndexTopics.out'
outputFileName2 = r'topicsDetails.out'
outputFileName3 = r'TitleLookup.out'

os.chdir(filepath)
stopwords = set(stopwords.words('english'))
lemmatizer = WordNetLemmatizer()
titleArray2 = []
final = []
invertedIndexTitle = {}
invertedIndexTitleLookup = {}


for item in os.listdir():
    itemTree = ET.parse(item)
    itemRoot = itemTree.getroot()
    queryId = itemRoot[0].text
    title = itemRoot[1].text
    
    ###Data Cleaning
    title = re.sub(r'[^\w\s]', '', title)
    
    ###Tokenization
    queryIdArray = [queryId]
    titleArray = nltk.word_tokenize(title)
    
    ###Remove Stop Words
    titleArray = [word for word in titleArray if not word in stopwords]
    
    #print(queryIdArray, titleArray, descArray)
    for i,word in enumerate(titleArray):
        titleArray[i] = titleArray[i].lower()
    
    shortTitleArray = []
    
    ###Lemmatization
    for word in titleArray:
        tempWord = lemmatizer.lemmatize(word)
        titleArray2.append(tempWord)
        shortTitleArray.append(tempWord)
        if tempWord in invertedIndexTitle:
            mainQuery = invertedIndexTitle[tempWord]
            if queryIdArray[0] in mainQuery.keys():
                occ = invertedIndexTitle[tempWord][queryIdArray[0]]
                occ = occ + 1
                invertedIndexTitle[tempWord][queryIdArray[0]] = occ
            else:
                invertedIndexTitle[tempWord].update({queryIdArray[0]: 1})
        else:
            invertedIndexTitle[tempWord] = {queryIdArray[0]: 1}
        if tempWord in invertedIndexTitleLookup:
            occ = invertedIndexTitleLookup[tempWord]
            occ = occ + 1
            invertedIndexTitleLookup[tempWord] = occ
        else:
            invertedIndexTitleLookup[tempWord] = 1
        
    final.append([queryIdArray,shortTitleArray])
    
   
#Sorting the index keys alphabetically for better search later
invertedIndexTitle = {k:v for k, v in sorted(invertedIndexTitle.items())}
print(invertedIndexTitle)


os.chdir(outputFilePath)

outputFile = open(outputFileName1, 'w')
outputFile.write("Inverted Index of Topic: \n" + str(invertedIndexTitle) + "\n")

outputFile = open(outputFileName2, 'w')
outputFile.write(str(final))

outputFile = open(outputFileName3, 'w')
outputFile.write("Lookup of Title: \n" + str(invertedIndexTitleLookup) + "\n")

outputFile.close()
