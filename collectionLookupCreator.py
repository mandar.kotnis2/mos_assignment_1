import os
import xml.etree.ElementTree as ET
import nltk
import re
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

filepath = r'D:\Mechanics of Search\Assignment 1\COLLECTION'
outputFilePath = r'D:\Mechanics of Search\Assignment 1\output 2'

invertedIndexCollections = {}

counter = 1

os.chdir(filepath)
stopwords = set(stopwords.words('english'))
lemmatizer = WordNetLemmatizer()
headlineArray2 = []
textArray2 = []
unique = []

for item in os.listdir():
    itemTree = ET.parse(item)
    itemRoot = itemTree.getroot()
    docId = itemRoot[0].text
    docId = str(docId)
    headline = itemRoot[1].text
    headline = str(headline)
    text = itemRoot[2].text
    text = str(text)
    text = text + headline
    
    unique = []
    
    ###Data Cleaning
    text = re.sub(r'[^\w\s]', '', text)
    
    
    ###Tokenization
    docIdArray = [docId]
    textArray = nltk.word_tokenize(text)
    
    for i,word in enumerate(textArray):
        textArray[i] = textArray[i].lower()
    
    shortTextArray = []
    
    ###Remove Stop Words
    textArray = [word for word in textArray if not word in stopwords]
    
    ###Lemmatization        
    for word in textArray:
        tempWord = lemmatizer.lemmatize(word)
        textArray2.append(tempWord)
        shortTextArray.append(tempWord)
            
    for word in shortTextArray:
        if word not in unique:
            unique.append(word)       
    
    for word in unique:
        if word in invertedIndexCollections:
            occ = invertedIndexCollections[word]
            occ = occ + 1
            invertedIndexCollections[word] = occ
        else:
            invertedIndexCollections[word] = 1
    
    print('{} of 12208 done'.format(counter))
    counter = counter + 1
            
            
print(invertedIndexCollections)

os.chdir(outputFilePath)
outputFile = open('IDFLookupCollection.out', 'w')
outputFile.write(str(invertedIndexCollections) + "\n")
outputFile.close()