import os
import math

def Sort(sub):
    sub.sort(key = lambda x: x[2], reverse=False)
    return sub

os.chdir(r'D:\Mechanics of Search\Assignment 1\output')

topicsDetailsFile = open('topicsDetails.out', 'r')
collectionsLookupFile = open('CollectionLookup.out', 'r')
idfLookupCollectionFile = open('IDFLookupCollection.out', 'r')
idfLookupTopicsFile = open('IDFLookupTopics.out', 'r')

outputFileName1 = r'invertedIndexCollections{}.out'
outputFileName2 = r'collectionsDetails{}.out'

readTopics = topicsDetailsFile.readline()
topicsDetailsFile.close()

readCollectionsLookup = collectionsLookupFile.readlines()
collectionsLookupFile.close()

readIdfLookupCollection = idfLookupCollectionFile.readlines()
idfLookupCollectionFile.close()

readIdfLookupToppic = idfLookupTopicsFile.readlines()
idfLookupTopicsFile.close()

topicsDetails = eval(readTopics)
idfLookupCollection = eval(readIdfLookupCollection[0])
idfLookupTopic = eval(readIdfLookupToppic[0])

rank = []
rankString = ''
lenSum = 6886391
lambdaValue = 0.1

overallLambdaScore = (1 - lambdaValue) / lambdaValue

#The length of all documents is calculated below to be 6886391. Commented to save time
'''
for i in range (1,14):
    collectionsDetailsFile = outputFileName2.format(i)
    invertedIndexCollectionFile = outputFileName1.format(i)
     
    collectionsDetailsFileName = open(collectionsDetailsFile, 'r')
     
    readCollections = collectionsDetailsFileName.readlines()
    collectionsDetailsFileName.close()
     
    collectionsDetails = eval(readCollections[0])    
     
    for currentCollection in collectionsDetails:
        docIdArray = currentCollection[0]
        texts = currentCollection[1]
        lenSum = lenSum + len(texts)
         
    print('{} done'.format(i))
         
print(lenSum)
'''

for topicDetails in topicsDetails:
    for r in rank:
        rankString = rankString + '{} man {} {} \n'.format(r[0], r[1], r[2])
    rank = []
    queryIdArray = topicDetails[0]
    titles = topicDetails[1]
    
    nTitles = [0] * len(titles)
    
    
    
    for i,title in enumerate(titles):
        nTitles[i] = titles.count(title)
    
    
    for i in range (1,14):
        collectionsDetailsFile = outputFileName2.format(i)
        invertedIndexCollectionFile = outputFileName1.format(i)
        
        collectionsDetailsFileName = open(collectionsDetailsFile, 'r')
        
        readCollections = collectionsDetailsFileName.readlines()
        collectionsDetailsFileName.close()
        
        collectionsDetails = eval(readCollections[0])    
        
        for currentCollection in collectionsDetails:
            docIdArray = currentCollection[0]
            texts = currentCollection[1]
            
            nText = [0] * len(titles)
            nCText = [0] * len(titles)
            pText = [0] * len(titles)
            lText = [0] * len(titles)
            prod = [0] * len(titles)
            
            for i,title in enumerate(titles):
                nText[i] = texts.count(title)
                if title in idfLookupTopic:
                    nCText[i] = idfLookupTopic[title]
                else:
                    nCText[i] = 0
                
            for i,title in enumerate(titles):
                pText[i] = nCText[i]/lenSum
                
            for i,title in enumerate(titles):
                if (pText [i] == 0):
                    lText[i] = 0
                else:
                    normalizationValue = nText[i]/len(texts)*pText[i]
                    lText[i] = math.log((1 + (overallLambdaScore * normalizationValue)), 10)
                    
            for i,title in enumerate(titles):
                prod[i] = lText[i] * nText[i]
                
            rank.append([queryIdArray[0], docIdArray[0], sum(prod)])
            rank = Sort(rank)
            if(len(rank) > 10):
                rank = rank[:10]
    print('Topic {} complete'.format(queryIdArray[0]))
                
            
rank = Sort(rank)
if(len(rank) > 10):
    rank = rank[:10]
for r in rank:
    rankString = rankString + '{} man {} {} \n'.format(r[0], r[1], r[2])
print(rankString)
print(rankString.count('\n'))

outputFile = open('LMS_trec_eval_mandar.out', 'w')
outputFile.write(rankString)
