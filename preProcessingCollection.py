import os
import xml.etree.ElementTree as ET
import nltk
import re
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

#Required to run nltk
#nltk.download('punkt')
#nltk.download('wordnet')
#nltk.download('stopwords')

#All the file path names which are likely to change when another user uses this
filepath = r'D:\Mechanics of Search\Assignment 1\COLLECTION'
outputFilePath = r'D:\Mechanics of Search\Assignment 1\output'
outputFileName1 = r'invertedIndexCollections{}.out'
outputFileName2 = r'collectionsDetails{}.out'

os.chdir(filepath)
stopwords = set(stopwords.words('english'))
lemmatizer = WordNetLemmatizer()
textArray2 = []
final = []
invertedIndexText = {}
invertedIndexTextLookup = {}
FileNumber = 1

#Function to print out Inverted Index and file details into two separate local files
def printoutput(FileNumber):
    os.chdir(outputFilePath)
    global invertedIndexText
    invertedIndexText = {k:v for k, v in sorted(invertedIndexText.items())}
    
    outputFile = open(outputFileName2.format(FileNumber), 'w')
    outputFile.write(str(final))
    outputFile.close()

    outputFile = open(outputFileName1.format(FileNumber), 'w')
    outputFile.write("Inverted Index of Text: \n" + str(invertedIndexText))
    outputFile.close()
    os.chdir(filepath)


for item in os.listdir():
    itemTree = ET.parse(item)
    itemRoot = itemTree.getroot()
    docId = itemRoot[0].text
    docId = str(docId)
    headline = itemRoot[1].text
    headline = str(headline)
    text = itemRoot[2].text
    text = str(text)
    text = text + headline
    
    #Print out the results into local files if 1000 collection files have been checked
    if len(final) >= 1000:
        printoutput(FileNumber)
        final = []
        invertedIndexText = {}
        FileNumber = FileNumber + 1
    
    
    ###Data Cleaning
    text = re.sub(r'[^\w\s]', '', text)
    
    
    ###Tokenization
    docIdArray = [docId]
    textArray = nltk.word_tokenize(text)
    
    ###Turning every word in the array into lower case
    for i,word in enumerate(textArray):
        textArray[i] = textArray[i].lower()
    
    shortTextArray = []
    
    ###Remove Stop Words
    textArray = [word for word in textArray if not word in stopwords]
    
    ###Lemmatization
    for word in textArray:
        tempWord = lemmatizer.lemmatize(word)
        textArray2.append(tempWord)
        shortTextArray.append(tempWord)
        
        if tempWord in invertedIndexText:
            mainQuery = invertedIndexText[tempWord]
            if docIdArray[0] in mainQuery.keys():
                occ = invertedIndexText[tempWord][docIdArray[0]]
                occ = occ + 1
                invertedIndexText[tempWord][docIdArray[0]] = occ
            else:
                invertedIndexText[tempWord].update({docIdArray[0]: 1})
        else:
            invertedIndexText[tempWord] = {docIdArray[0]: 1}
            
        if tempWord in invertedIndexTextLookup:
            occ = invertedIndexTextLookup[tempWord]
            occ = occ + 1
            invertedIndexTextLookup[tempWord] = occ
        else:
            invertedIndexTextLookup[tempWord] = 1
    
    final.append([docIdArray, shortTextArray])

printoutput(FileNumber)
print('Indexing Done')
os.chdir(outputFilePath)
outputFile = open('CollectionLookup.out', 'w')
outputFile.write("Lookup of Text: \n" + str(invertedIndexTextLookup))
outputFile.close()
