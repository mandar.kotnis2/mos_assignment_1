import os
import math
from math import sqrt

#Sort the rank by relevance
def Sort(sub):
    sub.sort(key = lambda x: x[2], reverse=True)
    return sub

def sumofsquare(arr):
    sumofsquare = 0
    for i in arr:
        sumofsquare = sumofsquare + (i * i)
    return sumofsquare

os.chdir(r'D:\Mechanics of Search\Assignment 1\output')

topicsDetailsFile = open('topicsDetails.out', 'r')
collectionsLookupFile = open('CollectionLookup.out', 'r')
idfLookupCollectionFile = open('IDFLookupCollection.out', 'r')
idfLookupTopicsFile = open('IDFLookupTopics.out', 'r')

outputFileName1 = r'invertedIndexCollections{}.out'
outputFileName2 = r'collectionsDetails{}.out'

readTopics = topicsDetailsFile.readline()
topicsDetailsFile.close()

readCollectionsLookup = collectionsLookupFile.readlines()
collectionsLookupFile.close()

readIdfLookupCollection = idfLookupCollectionFile.readlines()
idfLookupCollectionFile.close()

readIdfLookupToppic = idfLookupTopicsFile.readlines()
idfLookupTopicsFile.close()

topicsDetails = eval(readTopics)
idfLookupCollection = eval(readIdfLookupCollection[0])
idfLookupTopic = eval(readIdfLookupToppic[0])

rank = []
rankString = '' 

numOfDoc = 12208

collectionRelevance = {}

counter = 1

for i in range (1,14):
    collectionsDetailsFile = outputFileName2.format(i)
    
    collectionsDetailsFileName = open(collectionsDetailsFile, 'r')
    
    readCollections = collectionsDetailsFileName.readlines()
    collectionsDetailsFileName.close()
    
    collectionsDetails = eval(readCollections[0])
    
    for currentCollection in collectionsDetails:
        docIdArray = currentCollection[0]
        texts = currentCollection[1]
        
        #Check the number of occurences for each term in collection
        nTexts = [0] * len(texts)
        
        #Calculate tf for each term in collection
        tfTexts = [0] * len(texts)
            
        for i,text in enumerate(texts):
            tfTexts[i] = tfTexts[i] + texts.count(text)
            tfTexts[i] = tfTexts[i] / (len(texts))

        #Calculate idf for each term in collection   
        idfTexts = []
        
        for i, text in enumerate(texts):
            if text in idfLookupCollection:
                nTexts[i] = idfLookupCollection[text]
            else:
                nTexts[i] = 0
        
        for i,text in enumerate(texts):
            if nTexts[i] == 0:
                idfTexts.append(0)
            else:
                idfTexts.append(math.log((numOfDoc)/(nTexts[i])))

        #Calculate tf-idf for each term in collection     
        tfIdfTexts = [0] * len(texts)
        for i, text in enumerate(texts):
            tfIdfTexts[i] = tfTexts[i]  * idfTexts[i]
        
        
        docRelevance = 0
        docRelevance = docRelevance + sumofsquare(tfIdfTexts)
        finalDocRelevance = sqrt(docRelevance)
        collectionRelevance[docIdArray[0]] = finalDocRelevance
        print('Collection {} complete. {} out of {} complete'.format(docIdArray[0], counter, numOfDoc))
        counter = counter + 1
        
for topicDetails in topicsDetails:
    for r in rank:
        rankString = rankString + '{} man {} {} \n'.format(r[0], r[1], r[2])
    rank = []
    queryIdArray = topicDetails[0]
    titles = topicDetails[1]
    
    #Check the number of occurences for each term in query
    nTitle = [0] * len(titles)
    
    #Calculate tf of each term in query
    tfTitles = [0] * len(titles)
    
    #Calculating idf of each term in query
    idfTitle = [0] * len(titles)
    
    #Calculate tf of each term in query
    for i,title in enumerate(titles):
        tfTitles[i] = tfTitles[i] + titles.count(title)
        tfTitles[i] = tfTitles[i] / (len(titles))
        
    for i,title in enumerate(titles):
        if title in idfLookupTopic:
            nTitle[i] = idfLookupTopic[title]
        else:
            nTitle[i] = 0
        
    #Calculating IDF
    for i,title in enumerate(titles):
        if nTitle[i] == 0:
            idfTitle[i] = 0
        else:
            idfTitle[i] = math.log((numOfDoc) /(nTitle[i]))

    #Calculating tf-idf of each term in query      
    tfIdfTitles = [0] * len(titles)
    for i, title in enumerate(titles):
        tfIdfTitles[i] = tfTitles[i]  * idfTitle[i]
        
    titleRelevance = 0
    titleRelevance = titleRelevance + sumofsquare(tfIdfTitles)
    finalTitleRelevance = sqrt(titleRelevance)
    
    for i in range (1,14):
        collectionsDetailsFile = outputFileName2.format(i)
        invertedIndexCollectionFile = outputFileName1.format(i)
        
        collectionsDetailsFileName = open(collectionsDetailsFile, 'r')
        
        readCollections = collectionsDetailsFileName.readlines()
        collectionsDetailsFileName.close()
        
        collectionsDetails = eval(readCollections[0])
        
        for currentCollection in collectionsDetails:
            docIdArray = currentCollection[0]
            texts = currentCollection[1]
            
            tfTitlesCollection = [0] * len(titles)
            
            #Calculate tf of each term in query
            for i,title in enumerate(titles):
                tfTitlesCollection[i] = tfTitlesCollection[i] + texts.count(title)
                tfTitlesCollection[i] = tfTitlesCollection[i] / (len(titles))
                
            num = 0
            for i,title in enumerate(titles):
                num = num + (tfTitles[i] * idfTitle[i]) * (tfTitlesCollection[i] * idfTitle[i])
            den1 = finalTitleRelevance
            den2 = collectionRelevance[docIdArray[0]]
            ans = 0
            if den1 == 0 or den2 == 0:
                ans = 0
            else:
                ans = num / (den1 * den2)
            rank.append([queryIdArray[0], docIdArray[0], ans])
            rank = Sort(rank)
            if(len(rank) > 10):
                rank = rank[:10]
    print('Topic {} complete'.format(queryIdArray[0]))
    
rank = Sort(rank)
if(len(rank) > 10):
    rank = rank[:10]
for r in rank:
    rankString = rankString + '{} man {} {} \n'.format(r[0], r[1], r[2])
print(rankString)
print(rankString.count('\n'))

outputFile = open('vsm_trec_eval_mandar.out', 'w')
outputFile.write(rankString)
